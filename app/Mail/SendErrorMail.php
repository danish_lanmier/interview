<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendErrorMail extends Mailable
{
    use Queueable, SerializesModels;

    public $errors;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($error_data)
    {
        //

        $this->errors = $error_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.sendErrorMail')->with('errors',$this->errors);
    }
}
