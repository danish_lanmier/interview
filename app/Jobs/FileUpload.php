<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Validator;
use DB;
use App\Mail\SendErrorMail;
use Mail;

class FileUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $file;

    public $file_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filepath)
    {
        //

         $this->file = url('/storage/file/'.$filepath);

         $this->file_name = $filepath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
      
    $column =1;

    $errors = [];

    $required_error = [];

    $symbol_error = [];

    $insert_data = [];

    $headers = "";
       
    if (($handle = fopen($this->file, "r")) !== FALSE) {
    
        while (($data = fgetcsv($handle, 1000,)) !== false)
        {
          
            if($column==1){

                $headers = $data;
               
                if(count($data)!=config('customfile.column_no')){

                    $errors[] = ['invalid column in this csv'];

                    $this->email_send($errors);

                    //\Log::error('invalid column in this csv');
                    break;
                }
                 
            }
            
            for ($c=0; $c < count($data); $c++) {

                $is_error=false;

                $field = [];

                $required_validation = '';

                if($column==1){
                    
                   
                    if(config('customfile.header')[$c]!=$data[$c]){
                          
                       $errors[] = 'Header column ('.$data[$c].' at '.($c+1).' column) is incorrect in csv file';  
                    }
                     
                }

                if($column!=1){
                     
                    $row_data = ['key_name'=>$data[$c]];
                    
                    $required_validation = Validator::make($row_data, [
                        'key_name'=>'required',
                        
                     ]);  

                     
                     if($required_validation && $required_validation->fails()){
                           
                        $required_error[config('customfile.header')[$c]][]= $column;  

                        $is_error = true;

                       
                     }
                     
                     if(!empty($data[$c])){
                           
                        $symbol_validation = Validator::make($row_data, [
                            'key_name'=>"regex:/^[A-Za-z0-9 ]+$/",
                            
                         ]);  
                       
                         if($symbol_validation->fails()){
                           
                            $symbol_error[config('customfile.header')[$c]][]= $column;  

                            $is_error = true;
                         }  

                     }

                     

                     
                     if(config('customfile.header')[$c]==$headers[0]){
                           
                         $user_code = $is_error?'':$data[$c];
                     }

                     if(config('customfile.header')[$c]==$headers[1]){
                           
                        $user_name= $is_error?'':$data[$c];
                    }

                    if(config('customfile.header')[$c]==$headers[2]){
                           
                        $user_address= $is_error?'':$data[$c];
                    }

                    if($c==(config('customfile.column_no')-1)){
                          
                       
                        DB::table('user_data')->insert(['user_code'=>$user_code,'user_name'=>$user_name,'user_address'=>$user_address]);
                    }
                    


                }

                
                
             }


             

             


            $column++;
           
        }
        
       

         if(!empty($required_error)){
              
            foreach($required_error as $key=>$value){
                  
                $errors[] = $key.' is missing at row '.implode(',row ',$required_error[$key]);
            }
              
         }

         if(!empty($symbol_error)){
               
            foreach($symbol_error as $key=>$value){
                  
                $errors[] = $key.' contains symbols at row '.implode(',row ',$symbol_error[$key]);
            }
             
         }

         if(!empty($errors)){
              
             $this->email_send($errors);
         }

       }  
        fclose($handle);
    
       
     dd('successfully inserted');



    }

    public function email_send($error_data){
        
        foreach (['rahul@protracked.in', 'akshara@protracked.in'] as $recipient) {
            Mail::to($recipient)->send(new SendErrorMail($error_data));
        }  
        
       

       return true;
         
    }
}
