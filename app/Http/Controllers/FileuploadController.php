<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Jobs\FileUpload;

class FileuploadController extends Controller
{
    //

    public function file_upload(Request $request){

      
          
        $validator = Validator::make($request->all(), [
             'file'=>'required|mimes:csv',
             
          ]);         

        if ($validator->fails()) {

           return response()->json(['msg' => $validator->errors(), 'status' => false]);
        }

        $fileName = time().'.'.$request->file('file')->getClientOriginalExtension();

        $imageDirectory = 'public/file';

        $path = $request->file('file')->storeAs($imageDirectory, $fileName);
        
         
        dispatch(new FileUpload($fileName));

        return response()->json(['status'=>true,'message'=>'successfully upload']);
    }


}
