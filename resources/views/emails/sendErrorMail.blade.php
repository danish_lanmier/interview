@component('mail::message')
# Introduction

The body of your message.

@foreach($errors as $error)

<p>{{$error}}</p><br/>

@endforeach

Thanks,<br>
{{ config('app.name') }}
@endcomponent
